import os
import json
import googlemaps
from flask import Flask
app = Flask(__name__)

rides = {
    'campus_1': 0,
    'campus_2': 0
}

directions = {
    'campus_1': "USP São Carlos - Campus da Universidade de São Paulo",
    'campus_2': (-22.002331, -47.930211)
}

key = os.environ.get('GMAPS_API_KEY')
if key is None:
    print('Environment variable GMAPS_API_KEY not set')
    quit(1)

gmaps = googlemaps.Client(key=key)
# directions_result = gmaps.directions(campus_1, campus_2, mode='driving')

@app.route('/quero-carona/<campus>')
def registerRide(campus):
    global rides
    rides[campus] = rides[campus] + 1
    print(rides[campus], campus)
    return 'Ok'

@app.route('/quero-dar-carona/<campus>')
def getRiders(campus):
    global rides

    caroneiros = 3 if rides[campus] >= 3 else rides[campus]
    rides[campus] = rides[campus] - caroneiros

    campus_contrario = 'campus_1' if campus == 'campus_1' else 'campus_2'
    return json.dumps({
        'caroneiros': caroneiros,
        'route': gmaps.directions(directions[campus], directions[campus_contrario], mode='driving')
    })